module.exports = function (grunt) {
    require('time-grunt')(grunt);
    require('jit-grunt')(grunt, {
        useminprepare: 'grunt-usemin'
    });

    grunt.initConfig({
        sass: {
            dist: {
                files: [{
                    expand: true,
                    cwd: 'css',
                    src: ['*.scss'],
                    dest: 'css',
                    ext: '.css'
                }]
            }
        },

        watch: {
            files: ['css/*.scss'],
            tasks: ['css']
        },
        browserSyinc: {
            dev: {
                bsfiles: { //browser files
                    src: [
                        'css./*css',
                        '*.html',
                        'js/*.js'
                    ]
                },
                options: {
                    watchTask: true,
                    server: {
                        baseDir: './' //direcotrio base para nuestro serividor
                    }
                }
            }
        },
        
        imagemin: {
            dynamic: {
                files: [{
                    expand: true,
                    cwd: './',
                    src: 'img/*.{png,gif,jpg,jepg',
                    dest: 'dist/'
                }]
            }
        },

        copy: {
            html: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: './',
                    src: ['*.html'],
                    dest: 'dist'
                }]
            },
        fonts: {
            files: [{
                //for font-awesone
                    expand: true,
                    dot: true,
                    cwd: 'node_modules/open-iconic/font',
                    src: ['fonts/*.*'],
                    dest: 'dist'
                }]
             }
        },

        clean: {
            buil: {
                src: ['dist/']
            }
        },

        cssmin: {
            dist: {}
        },

        uglify: {
            dist: {}
        },

        filerew: {
            options: {
                encoding: 'utf8',
                algorithm: 'md5',
                lengt: 20
            },
       
            release: {
                // filerew:release hashes(md5) all assents (images, js and css)
                // in dist directory
                files:[{
                    src: [
                        'dist/js/�.js',
                        'dist/css/*.css',
                    ]
                }]
            }
        },

        concat: {
            options: {
                separator: ':'
            },
            dist:{}
        },
        
        useminPrepare: {
            foo: {
                dest: 'dist',
                src: ['index.html', 'about.html', 'colores.html', 'contacto.html']
            },
            options: {
                flow: {
                    steps: {
                        css: ['cssmin'],
                        js: ['uglify']
                },
                    post: {
                        css: [{
                            name: 'cssmin',
                            createConfig: function (context, black) {
                                var generated = context.options.generated;
                                generated.options = {
                                    keepSpecialComments: 0,
                                    rebase: false
                                }
                            }
                        }]
                    }
                }
            },

            usemin: {
                html: ['dist/index.html', 'dist/about.html', 'dist/colores.html', 'dist/contacto.html'],
                options: {
                    assetsDir: ['dist', 'dist/css', 'dist/js']
                }
            }

        },
        
           
    });

    grunt.registerTask('css', ['sass']);
    grunt.registerTask('default', ['browserSync', 'wath']);
    grunt.registerTask('img:compress', ['imagemin']);
    grunt.registerTask('build', [
        'clean',
        'copy',
        'imagemin',
        'useminPrepare',
        'concat',
        'cssmin',
        'uglify',
        'filerev',
        'usemin'
    ]);

};